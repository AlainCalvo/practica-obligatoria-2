using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBox : MonoBehaviour
{
    #region Variables

    [Header("---REFERENCIAS---")]
    public GameObject fill;
    public GameObject empty;
    public GameObject contextual;
    GameManager gm;
    PlayerController pc;
    GrowingWater gw;

    //Box usage
    public bool interactable;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        gw = FindObjectOfType<GrowingWater>();
        pc = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        if (pc.interact && interactable || gm.boxOpened) UseBox(); // If we are in the box and interact with it, the box opens.
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Open the box. 
    /// </summary>
    void UseBox()
    {
        empty.SetActive(true); 
        fill.SetActive(false);
        contextual.SetActive(false);
        if (!gm.boxOpened) gw.activated = true; //Activate water movement.
        gm.boxOpened = true; //We inform the GameManager that the box has been opened.
    }

    /// <summary>
    /// We reset the box in case of death by active water.
    /// </summary>
    public void Refill()
    {
        empty.SetActive(false);
        fill.SetActive(true);
        gm.boxOpened = false; //We inform the GameManager that the box is closed again..
        pc = FindObjectOfType<PlayerController>(); //We take the PlayerController from the new instance of the player after his death, in order to recognize the interaction between the two.
        gw.fillTheBox = false; //We inform the water that the box has been reset.
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter2D(Collider2D collision)
    {
       if(collision.tag == "Player" && gm.boxOpened == false)
       {
            contextual.SetActive(true); //We show the contextual poster.
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        pc = FindObjectOfType<PlayerController>();
        if (collision.tag == "Player" && gm.boxOpened == false) 
            interactable = true; //We make the box interactive to be able to open it.
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player" && gm.boxOpened == false)
        {
            contextual.SetActive(false); //We hide the contextual banner if we move away from the closed box.
            interactable = false; //It is not possible to interact with the box when moving away.
        }  
    }

    #endregion
}
