using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorPlatform : MonoBehaviour
{
    #region Variables

    //Referencias
    GameManager gm;
    PlayerController pc;
    Animator anim;

    [Header("---OBJETOS---")]
    public GameObject on;
    public GameObject off;
    public GameObject contextual;

    bool interactable;

    #endregion

    #region Main Methods

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Play opening sound, and we load the end of game screen.
    /// </summary>
    /// <returns></returns>
    IEnumerator EndGame()
    {
        SoundManager.PlaySound("Door");
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // Check if we already have the keys and if so, allow us to open the door.
        gm = FindObjectOfType<GameManager>();
        if(collision.tag == "Player" && gm.keys == 2)
        {
            off.SetActive(false);
            on.SetActive(true);
            contextual.SetActive(true);
            interactable = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        //Open the door
        pc = FindObjectOfType<PlayerController>();
        if (collision.tag == "Player" && gm.keys == 2)
        {
            if(interactable && pc.interact)
            {
                contextual.SetActive(false);
                anim.SetBool("Open", true);
                StartCoroutine(EndGame());
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {   
        //"Deactivate" the door
        if (collision.tag == "Player" && gm.keys == 2)
        {
            off.SetActive(true);
            on.SetActive(false);
            contextual.SetActive(false);
            interactable = false;
        }
    }

    #endregion

}
