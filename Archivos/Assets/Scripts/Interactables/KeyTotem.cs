using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyTotem : MonoBehaviour
{
    #region Variables

    public GameObject interact;
    public GameObject key;

    public bool interactable;
    
    int numberKey;

    public bool thisKey;


    GameManager gm;
    PlayerController pc;

    #endregion

    #region Main Methods

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        pc = FindObjectOfType<PlayerController>();
        numberKey = GetComponentInChildren<KeyNumberç>().keyNumber;        
    }

    private void Start()
    {
        //Unify with GameManager which key is for the save system.
        if (numberKey == 1) thisKey = gm.firstKey;
        if (numberKey == 2) thisKey = gm.secondKey;

        //Deactivate the key if it is caught.
        if (thisKey) key.SetActive(false);
    }

    private void Update()
    {
        if (pc.interact && interactable && !thisKey) PickTheKey();       
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Manages the acquisition of keys
    /// </summary>
    void PickTheKey()
    {
        interact.SetActive(false); //We remove the contextual banner.
        key.SetActive(false); //We remove the screen key.
        gm.keys = gm.keys + 1; //We add a key to the GameManager counter.
        if (numberKey == 1) gm.firstKey = true;
        if (numberKey == 2) gm.secondKey = true;
        thisKey = true;
    }

    #endregion

    #region Detection Methods

    private void OnTriggerEnter2D(Collider2D collision)
    {
        gm = FindObjectOfType<GameManager>();
        pc = FindObjectOfType<PlayerController>();
        if (thisKey == false && collision.tag == "Player")
        {            
            interact.SetActive(true); //We show the contextual poster.
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        pc = FindObjectOfType<PlayerController>();
        if(thisKey == false && collision.tag == "Player")
        {
            interactable = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        interact.SetActive(false);
        interactable = false;
    }

    #endregion
}
