using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    #region Variables

    public static DataManager instance;

    public int active;

    #endregion

    #region Main Methods

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        else if(instance != null)
        {
            Destroy(gameObject);
        }        
    }

    private void Start()
    {
        DontDestroyOnLoad(gameObject);
        active = PlayerPrefs.GetInt("Active", 0); //Loads if there is data stored.
    }

    #endregion

    #region Custom Methods

    public void Saved(int value) //Register that there are saved data (to be able to continue or not to continue the game).
    {
        PlayerPrefs.SetInt("Active", value); 
    }

    public void Hearts(float quanty) //Saves the current life.
    {
        PlayerPrefs.SetFloat("HeartsQuanty", quanty);
    }

    public void SpawnReferenceX(float posX) //Save Position in X.
    {
        PlayerPrefs.SetFloat("PositionX", posX);
    }

    public void SpawnReferenceY(float posY) //Save Position in Y.
    {
        PlayerPrefs.SetFloat("PositionY", posY);
    }

    public void BoxOpened(bool value) //Save if the weapon has been obtained.
    {
        PlayerPrefs.SetInt("BoxOpened", value ? 1:0);
    }

    public void FirstKey(bool value) //Save if the first key has been obtained.
    {
        PlayerPrefs.SetInt("FirstKey", value ? 1 : 0);
    }

    public void SecondKey(bool value) //Save if the second key has been obtained.
    {
        PlayerPrefs.SetInt("SecondKey", value ? 1 : 0);
    }

    public void Keys(int quanty) //Saves the number of keys obtained.
    {
        PlayerPrefs.SetInt("Keys", quanty);
    }

    #endregion
}
