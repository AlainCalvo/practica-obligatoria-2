using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Variables

    public bool itsDead;
   
    [SerializeField]
    GameObject prefabPlayer;
    Vector2 spawnPoint;
    PlayerController pc;
    GameObject player;

    PauseMenu pause;
   
    [Header("---WEAPON---")]
    public bool boxOpened; //Apertura de la caja del arma
    public bool gainedWeapon= false;

    [Header("---REFERENCIAS---")]
    public GameObject heartOne;
    public GameObject heartTwo;
    public GameObject heartThree;
    public GameObject weapon;


    //Puerta
    public bool firstKey;
    public bool secondKey;
    public int keys;

    #endregion

    #region Main Methods

    private void Awake()
    {
        Respawn();
    }

    private void Start()
    {
        pause = FindObjectOfType<PauseMenu>();
        boxOpened = PlayerPrefs.GetInt("BoxOpened", 0) == 1? true : false; //Check whether you have the weapon or not.
        firstKey = PlayerPrefs.GetInt("FirstKey", 0) == 1 ? true : false; //Check whether you have the first key or not.                                                            
        secondKey = PlayerPrefs.GetInt("SecondKey", 0) == 1 ? true : false; //Check whether you have the second key or not.
        keys = PlayerPrefs.GetInt("Keys", 0); //Check the number of keys obtained.
    }

    private void Update()
    {
        VisibilityCursor();
        Respawn();
        LifeControl();
        if (boxOpened) WeaponAdquired();
        if (!boxOpened) WeaponLost();
        if (pc.weapon && pc.gunOut) weapon.SetActive(true);
        else weapon.SetActive(false);
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Control players life
    /// </summary>
    void LifeControl()
    {
        pc = FindObjectOfType<PlayerController>(); 
       
        if (pc.life == 0) //Dead
        {
            itsDead = true; //Activate Respawn.
            heartOne.SetActive(false); //Lose the last heart.
        }
        if(pc.life == 3)
        {
            heartOne.SetActive(true);
            heartTwo.SetActive(true);
            heartThree.SetActive(true);
        }
        if(pc.life == 2)
        {
            heartOne.SetActive(true);
            heartTwo.SetActive(true);
            heartThree.SetActive(false);
        }
        if(pc.life == 1)
        {
            heartOne.SetActive(true);
            heartTwo.SetActive(false);
            heartThree.SetActive(false);
        }
    }

    /// <summary>
    /// Destroys the player on death and creates him at spawn point
    /// </summary>
    void Respawn()
   {        
        itsDead = false;
        spawnPoint = new Vector2(PlayerPrefs.GetFloat("PositionX", 0f), PlayerPrefs.GetFloat("PositionY", -4.25f)); // We determine where to make the player appear.        
        player = GameObject.FindGameObjectWithTag("Player");
        if (itsDead == true || player == null)
        {
            Instantiate(prefabPlayer, spawnPoint, Quaternion.identity);
            pc = FindObjectOfType<PlayerController>();
        }
   }

    /// <summary>
    /// Activates the "event" of obtaining the weapon
    /// </summary>
    void WeaponAdquired()
    {        
        pc.armed = true; // Activates the arming condition.
        pc.weapon = true; // Activates weapon animations.
    }

    /// <summary>
    /// We remove the armed condition if we have not managed to escape with the gun.
    /// </summary>
    void WeaponLost()
    {        
        pc.armed = false; // We disarm the character.
        pc.weapon = false; // We return to animations without a weapon.
    }


    void VisibilityCursor()
    {
        if(SceneManager.GetActiveScene().buildIndex != 0)
        {
            if (pause.gameIsPaused) Cursor.lockState = CursorLockMode.None;
            else Cursor.lockState = CursorLockMode.Locked;
        }
    }
    #endregion
}
