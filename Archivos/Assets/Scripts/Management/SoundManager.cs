using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    #region Variables

    static AudioSource audioSource;

    static AudioClip growingWater;
    static AudioClip trapActived;
    static AudioClip shoot;
    static AudioClip chomper;
    static AudioClip spitter;
    static AudioClip column;
    static AudioClip drop;
    static AudioClip door;

    #endregion

    #region Main Methods

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        growingWater = Resources.Load<AudioClip>("GrowingWater");
        trapActived = Resources.Load<AudioClip>("TrapActive");
        shoot = Resources.Load<AudioClip>("Shoot");
        chomper = Resources.Load<AudioClip>("Chomper");
        spitter = Resources.Load<AudioClip>("Spitter");
        column = Resources.Load<AudioClip>("Column");
        drop = Resources.Load<AudioClip>("Drop");
        door = Resources.Load<AudioClip>("Door");
    }

    #endregion

    #region Custom Methods

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "GrowingWater":
                audioSource.PlayOneShot(growingWater);
                break; 
            case "TrapActive":
                audioSource.PlayOneShot(trapActived);
                break;
            case "Shoot":
                audioSource.PlayOneShot(shoot);
                break;
            case "Chomper":
                audioSource.PlayOneShot(chomper);
                break;
            case "Spitter":
                audioSource.PlayOneShot(spitter);
                break;
            case "Column":
                audioSource.PlayOneShot(column);
                break;
            case "Drop":
                audioSource.PlayOneShot(drop);
                break;
            case "Door":
                audioSource.PlayOneShot(door);
                break;

        }
    }

    #endregion
}
