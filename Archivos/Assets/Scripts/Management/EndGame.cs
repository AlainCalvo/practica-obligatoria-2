using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGame : MonoBehaviour
{
    #region Variables

    DataManager dm;

    #endregion

    #region Main Methods

    void Start()
    {
        dm = FindObjectOfType<DataManager>();
        StartCoroutine(BackToMenu());
    }

    #endregion

    #region Main Methods

    /// <summary>
    /// Return to the main menu after finishing.
    /// </summary>
    /// <returns></returns>
    IEnumerator BackToMenu()
    {
        dm.active = 0;
        yield return new WaitForSeconds(5f);
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }

    #endregion
}
