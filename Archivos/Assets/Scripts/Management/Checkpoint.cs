using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour
{
    #region Variables

    //Referencias.
    GameManager gm;
    PlayerController pc;
    DataManager dm;

    //Feedback visual.
    public GameObject on;
    public GameObject off;
    public GameObject saved;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
        dm = FindObjectOfType<DataManager>();        
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Save data in prefab and give feedback
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
       if (collision.gameObject.tag == "Player")
        {
            on.SetActive(true);
            off.SetActive(false);
            saved.SetActive(true);
            dm.active = 1; //Indicates that data has already been saved to the main menu.
            pc = FindObjectOfType<PlayerController>();

            DataManager.instance.Hearts(pc.life); //Saves the player's life.
            DataManager.instance.SpawnReferenceX(pc.transform.position.x); //X position of the player.       
            DataManager.instance.SpawnReferenceY(pc.transform.position.y); //Y position of the player. 
            DataManager.instance.BoxOpened(gm.boxOpened); //If the weapon has been obtained.
            DataManager.instance.Saved(dm.active); //If there is saved data.
            DataManager.instance.FirstKey(gm.firstKey); //If the first key has been obtained.
            DataManager.instance.SecondKey(gm.secondKey); //If the second key has been obtained.
            DataManager.instance.Keys(gm.keys); //The number of keys.
        }
    }

    #endregion

    #region Detection Methods

    /// <summary>
    /// Disables visual feedback.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerExit2D(Collider2D collision)
    {
        off.SetActive(true);
        on.SetActive(false);
        saved.SetActive(false);
    }

    #endregion
}
