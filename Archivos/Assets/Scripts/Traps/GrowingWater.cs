using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowingWater : MonoBehaviour
{
    #region Variables

    //Water movement.
    GameObject maxLevel;

    Vector3 startPosition;

    public float waterSpeed;
    float timer = 0;

    public bool activated;
    bool waterFall;

    //Box.
    WeaponBox wb;

    public bool fillTheBox;

    //Player.
    bool disarm;

    //Audio
    public GameObject waterSound;

    #endregion

    #region Main Methods

    private void Awake()
    {
        wb = FindObjectOfType<WeaponBox>();
        startPosition = transform.position;
        maxLevel = GameObject.Find("MaxLevel");        
    }

    private void Update()
    {
        //The water rise is activated.
        if (activated == true)
        {
            disarm = true; //We make the player lose his weapon if he dies with this water while it is active.

            if (timer < 1.5f) timer += .5f * Time.deltaTime; //We create a small margin from the time we activate the water until it starts to rise (grace time to start escaping).
            if (timer >= 1.5f && transform.position.y <= maxLevel.transform.position.y && waterFall == false)
            {
                waterSound.SetActive(true);
                transform.Translate(Vector3.up * waterSpeed * Time.deltaTime, Space.Self);
                timer = 4;
            }

            if (transform.position.y >= maxLevel.transform.position.y) waterFall = true; //It has reached its high point and must now go down.

            if (waterFall) transform.Translate(Vector3.down * 5 * Time.deltaTime, Space.Self);

            if(transform.position.y < startPosition.y)
            {
                waterSound.SetActive(false);
                waterFall = false; //We stop the descent.
                timer = 0;
                activated = false; //We deactivate the water.
                disarm = false; //At this point, if the player dies with this water, he will not lose the weapon if he has it.
                transform.position = new Vector2(transform.position.x, startPosition.y); //We place it at the point of origin in order to reactivate it.
            }
        }

        if (fillTheBox) wb.Refill(); //We call the "reset" function of the box if the player has died losing the weapon.
    }

    #endregion

    #region Detection Methods

    /// <summary>
    /// If the player is killed during the ascent, the water is lowered to its original position and prompts to reset the gun box.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Player" && disarm == true)
        {
            fillTheBox = true; // We tell the box to "reset".
            waterFall = true; //The water goes down without needing to reach the highest point.
        }
    }

    #endregion
}
