using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadlyColum : MonoBehaviour
{
    #region Variables
    Rigidbody2D rb2d;
    CapsuleCollider2D cc2d;

    bool active;
    bool impact;
    bool isPlaying;
    #endregion

    #region Main Methods

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        cc2d = GetComponent<CapsuleCollider2D>();
    }

    private void FixedUpdate()
    {
        if (active) KillThemAll();
        if (impact) GoingUp();
        else if (!active && !impact) rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// The spiked column falls.
    /// </summary>
    void KillThemAll()
    {
        if (!isPlaying) MakeNoise();
        rb2d.constraints = RigidbodyConstraints2D.None; // We freeze the rigibody to avoid problems.
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb2d.constraints = RigidbodyConstraints2D.FreezePositionX;
        rb2d.velocity = new Vector2(0, -7); // We move it down.
    }

    /// <summary>
    /// We return the columana to its original position to be able to activate it again.
    /// </summary>
    void GoingUp()
    {        
        rb2d.constraints = RigidbodyConstraints2D.None; // Freeze to avoid failures.
        rb2d.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb2d.constraints = RigidbodyConstraints2D.FreezePositionX;
        rb2d.velocity = new Vector2(0, 10); // We move it up.
    }

    void MakeNoise()
    {
        SoundManager.PlaySound("Column");
        isPlaying = true;        
    }

    #endregion

    #region Detection Methods

    /// <summary>
    /// The trap is activated.
    /// </summary>
    /// <param name="collision"></param>
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player") active = true;
    }

    /// <summary>
    /// Brakes downhill and uphill.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        isPlaying = false;
        if (collision.gameObject.layer == 8 || collision.gameObject.tag == "Player")
        {
            active = false;
            impact = true;
        }

        if (collision.gameObject.layer == 9) impact = false;
    }

    #endregion
}
