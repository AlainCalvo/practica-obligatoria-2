using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalPlatform : MonoBehaviour
{
    #region Variables

    Rigidbody2D rb2d;
    bool movingRight;

    #endregion

    #region Main Methods

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        //We change direction when we reach the extreme.
        if (movingRight) rb2d.velocity = new Vector2(2, 0);
        else rb2d.velocity = new Vector2(-2, 0);
    }

    #endregion

    #region Detection Methods

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 8 || collision.gameObject.layer == 9) movingRight = !movingRight;
    }

    #endregion
}
