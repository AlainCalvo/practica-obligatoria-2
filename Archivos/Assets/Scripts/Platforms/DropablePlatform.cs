using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropablePlatform : MonoBehaviour
{
    #region Variables

    Rigidbody2D rb2d;

    Vector2 startPosition;

    public float timer = 0;

    public bool downing;

    #endregion

    #region Main Methods

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        startPosition = transform.position;
    }

    private void FixedUpdate()
    {
        if (!downing && transform.position.y < startPosition.y) rb2d.velocity = new Vector2(0, 2);
        if (transform.position.y > startPosition.y)
        {
            rb2d.constraints = RigidbodyConstraints2D.FreezeAll;
            
        }
        if (downing) DropThePlatform();
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// We dropped the platform.
    /// </summary>
    void DropThePlatform()
    {        
       timer += Time.deltaTime;

        if (timer >= 1f) //We give a small margin before it falls off.
        {
            rb2d.constraints = RigidbodyConstraints2D.None;
            rb2d.constraints = RigidbodyConstraints2D.FreezeRotation | RigidbodyConstraints2D.FreezePositionX; ;
            
            rb2d.velocity = new Vector2(0, -5);
        }
    }

    #endregion

    #region Detection Methods

    /// <summary>
    /// Sound management.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            downing = true;
            SoundManager.PlaySound("Drop");
        }
        if (collision.gameObject.layer == 8)
        {
            downing = false;
            timer = 0;
        }
    }
    #endregion
}
