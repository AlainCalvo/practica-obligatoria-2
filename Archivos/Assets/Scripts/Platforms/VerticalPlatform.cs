using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalPlatform : MonoBehaviour
{
    #region Variables

    Rigidbody2D rb2d;
    CircleCollider2D cc2D;

    public float moveSpeed;
    float maxHeight;
    float minHeight;

    int direction = 1;

    #endregion

    #region Main Methods

    private void Awake()
    {
        rb2d = GetComponent<Rigidbody2D>();
        maxHeight = transform.position.y + GetComponent<CircleCollider2D>().radius;
        minHeight = transform.position.y - GetComponent<CircleCollider2D>().radius;
        cc2D = GetComponent<CircleCollider2D>();
    }

    private void Start()
    {
        cc2D.enabled = false; //Deactivate the circlecollider2D after taking the limits because it affected the player's physics when entering it.
    }

    private void FixedUpdate()
    {
        rb2d.velocity = new Vector2(rb2d.velocity.x, moveSpeed * direction);

        if (transform.position.y > maxHeight) direction = -1;
        if (transform.position.y < minHeight) direction = 1;
    }

    #endregion
}
