using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
#region Variables

    //References.
    Rigidbody2D rb2d;
    PlayerController pc;
    SpriteRenderer sr;

    bool orientation;

    #endregion

    #region Main Methods

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        pc = FindObjectOfType<PlayerController>();
        sr = GetComponentInChildren<SpriteRenderer>();
        orientation = pc.facingRight;
    }

    private void Update()
    {
        if (orientation == false)
        {
            sr.flipX = true;
            rb2d.AddForce(-transform.right * .02f, ForceMode2D.Impulse); 
        }
        else rb2d.AddForce(transform.right * .02f, ForceMode2D.Impulse);

        Destroy(this.gameObject, .8f);
    }

    #endregion

    #region Detection Methods

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(this.gameObject);
    }

    #endregion
}


