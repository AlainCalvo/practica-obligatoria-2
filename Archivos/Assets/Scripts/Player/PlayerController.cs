using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    #region Variables

    //Weapon
    public bool armed;
    public bool weapon;
    public bool gunOut = true;
    public Transform leftShootPoint;
    public Transform rigthShootPoint;
    public GameObject bullet;    

    //Platforms

    float platfomSpeedX;
    float platfomSpeedY;
    public bool onPlatform;

    //Collider
    BoxCollider2D playerCollider;
    Vector2 colliderStartSize;
    Vector2 colliderStartOffset;  

    //Life
    public float life = 3;

    //Interaction
    public bool interact;

    [Header("---Components---")]

    public LayerMask groundLayer;
    Animator anim;
    Rigidbody2D rb2d;
    SpriteRenderer sr;

    GameManager gm;

    [Header("---Hor.Movem.---")]

    public float forceX;
    Vector2 direction;
    public bool facingRight = true;

    [Header("---Vert.Movem.---")]

    public float jumpForce;
    public bool movJump;
    public float jumpDelay = 0.25f;
    public float jumpTimer;

    [Header("---Physics---")]

    public float maxSpeed;

    [Header("---Ground Check---")]

    public bool onGround;
    public float groundLength;
    public Vector3 colliderOffset;

    #endregion

    #region Main Methods
    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponentInChildren<Animator>();
        sr = GetComponentInChildren<SpriteRenderer>();

        gm = FindObjectOfType<GameManager>();

        playerCollider = GetComponent<BoxCollider2D>();
        colliderStartOffset = GetComponent<BoxCollider2D>().offset;
        colliderStartSize = GetComponent<BoxCollider2D>().size;

        
        life = PlayerPrefs.GetFloat("HeartsQuanty", 3f);
    }

    private void Update()
    {       
        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));

        onGround = Physics2D.Raycast(transform.position + colliderOffset, Vector2.down, groundLength, groundLayer) || Physics2D.Raycast(transform.position - colliderOffset, Vector2.down, groundLength, groundLayer);
        movJump = Input.GetButtonDown("Jump");

        if (movJump)
            jumpTimer = Time.time + jumpDelay;
        anim.SetBool("onGround", onGround);

        if (onGround) anim.SetBool("hasJump", false);

        if (armed)
        {
            GunInOut();
            Shoot();
        }

        if (onGround && Input.GetKey(KeyCode.S))
        {
            Crouch();
            playerCollider.size = new Vector2(.4f, .5f);
            playerCollider.offset = new Vector2(0, 0.185f);
        }

        else
        {
            anim.SetBool("crouch", false);
            rb2d.constraints = RigidbodyConstraints2D.None; // We release the movement.
            rb2d.constraints = RigidbodyConstraints2D.FreezeRotation; // We block the rotation.
            playerCollider.size = colliderStartSize;
            playerCollider.offset = colliderStartOffset;
        }

        Interact();

        if (life == 0) Die();        
    }

    private void FixedUpdate()
    {
        Move(direction.x);
        if (jumpTimer > Time.time && onGround) Jump();
    }
    #endregion

    #region Custom Methods

    /// <summary>
    /// Movement
    /// </summary>
    /// <param name="horizontal"></param>
    private void Move(float horizontal)
    {
        if(onPlatform) rb2d.velocity = new Vector2(forceX * direction.x + platfomSpeedX, rb2d.velocity.y);
        else rb2d.velocity = new Vector2(forceX * direction.x, rb2d.velocity.y);

        if ((horizontal > 0 && !facingRight) || (horizontal < 0 && facingRight)) Flip();

        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
        anim.SetFloat("absX", Mathf.Abs(direction.x));        
    }

    /// <summary>
    /// Jump.
    /// </summary>
    private void Jump()
    {        
        rb2d.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
        anim.SetBool("hasJump", true);
    }


    /// <summary>
    /// Sprite rotation.
    /// </summary>
    void Flip()
    {
        facingRight = !facingRight;
        sr.flipX = facingRight ? false : true;
    }
       
    /// <summary>
    /// Crouch
    /// </summary>
    void Crouch()
    {
        anim.SetBool("crouch", true);
        if(!onPlatform) rb2d.constraints = RigidbodyConstraints2D.FreezePosition; //We prevent it from moving while crouched if we are not on a moving platform.
    }

    /// <summary>
    /// Interacting with objects
    /// </summary>
    void Interact()
    {
        if (Input.GetKeyDown(KeyCode.F))
            interact = true;
        if (Input.GetKeyUp(KeyCode.F))
            interact = false;
    }

    /// <summary>
    /// Death
    /// </summary>
    public void Die()
    {
        SceneManager.LoadScene(1); //Reload last checkpoint     
    }

    /// <summary>
    /// Equip-unequip weapon if we have obtained it
    /// </summary>
    void GunInOut()
    {
        if (Input.GetKeyDown(KeyCode.O)) gunOut = !gunOut;
        if (weapon == true && gunOut == true) //Draw.
        { 
            anim.SetBool("gunActive", true);
        }
        if (weapon == true && gunOut == false) //Holster
        {
            anim.SetBool("gunActive", false);
        }
    }

    /// <summary>
    /// Shooting with equipped weapon
    /// </summary>
    void Shoot()
    {
        //Variables to instantiate the bullet
        GameObject go;
        Vector3 leftosition = leftShootPoint.transform.position;
        Vector3 rigthPosition = rigthShootPoint.transform.position;

        if (gunOut)
        {
            if (Input.GetMouseButtonDown(0))
            {
                SoundManager.PlaySound("Shoot");
                //Depending on where we are looking at, we instantiate a bullet on one side or the other.
                if (facingRight) go = Instantiate(bullet, rigthPosition, Quaternion.identity);
                if (!facingRight) go = Instantiate(bullet, leftosition, Quaternion.identity);
            }
        }
    }
    #endregion

    #region Detection Methods
     
    private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.down * groundLength);
            Gizmos.DrawLine(transform.position - colliderOffset, transform.position - colliderOffset + Vector3.down * groundLength);
        }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 4 || collision.gameObject.tag == "Spicke") //Instant death by spikes or falling into the water.
        {
            gm.itsDead = true;
            Die();
        }
        if (collision.gameObject.tag == "Enemy") //Progressive loss of life due to enemy impact.
        {
            anim.SetBool("hurt", true);
            life -= 1;
        }         
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Mobile") // Modification of physics to stay on mobile platform.
        {
            platfomSpeedX = collision.gameObject.GetComponent<Rigidbody2D>().velocity.x;
            onPlatform = true;
            rb2d.velocity = new Vector2 (forceX * direction.x + platfomSpeedX, rb2d.velocity.y);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy") //Activates impact animation
        {
            anim.SetBool("hurt", false);
        }

        if (collision.gameObject.tag == "Mobile") onPlatform = false; //Modify physics when leaving the mobile platforms.
    }
    #endregion
}


