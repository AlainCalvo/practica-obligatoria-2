using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour
{
    #region Variables

    PinkMonster pm;

    #endregion

    #region Main Methods

    private void Start()
    {
        pm = GetComponentInParent<PinkMonster>();        
    }

    #endregion

    #region Detection Methods

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet") pm.life -= 1;
    }

    #endregion
}
