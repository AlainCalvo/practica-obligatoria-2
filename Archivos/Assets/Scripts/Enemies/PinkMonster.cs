using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkMonster : MonoBehaviour
{
    #region Variables

    Animator anim;
    Rigidbody2D rb2d;
    PlayerController pc;

    public float life;
    float limitPatrolRight;
    float limitPatrolLeft;
    float time = 0;
    bool stillPlaying;
    

    public float speedPatrol;
    public float ratioPersecution;
    int direction = 1;

   
    public enum enemyBehaviourType { passive, persecution, attack };

    public enemyBehaviourType behaviour = enemyBehaviourType.passive;

    public float persecutionZoneEnter;
    public float persecutionZoneExit;
    public float attackZone;

    float distanceToPlayer;
    float vertDistanceToPlayer;
    GameObject player;

    #endregion

    #region Main Methods

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
        limitPatrolRight = transform.position.x + GetComponent<CircleCollider2D>().radius;
        limitPatrolLeft = transform.position.x - GetComponent<CircleCollider2D>().radius;
        pc = FindObjectOfType<PlayerController>();
        life = 3;
    }

    private void Update()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        
        if (life == 0)
        {
            rb2d.constraints = RigidbodyConstraints2D.FreezeAll; //Prevents it from moving due to the lack of friction on the ground while doing the dying animation.
            anim.SetBool("dead", true); //Activates the death animation.
            StartCoroutine(GetOut()); //Disabled until respawned.
        }
    }

    private void FixedUpdate()
    {      
        if(player != null)
        {            
            distanceToPlayer = Mathf.Abs(player.transform.position.x - transform.position.x);
            vertDistanceToPlayer = Mathf.Abs(player.transform.position.y - transform.position.y);

            switch (behaviour)
            {
                //Patrol
                case enemyBehaviourType.passive:
                    anim.SetBool("persecution", false);
                    anim.SetBool("move", true);
                    rb2d.velocity = new Vector2(speedPatrol * direction, rb2d.velocity.y);

                    if (transform.position.x < limitPatrolLeft) direction = 1;
                    if (transform.position.x > limitPatrolRight) direction = -1;

                    if (distanceToPlayer < persecutionZoneEnter && vertDistanceToPlayer <= .5f && vertDistanceToPlayer >= -.5f) behaviour = enemyBehaviourType.persecution;
                    break;

                    //Follow player
                case enemyBehaviourType.persecution:
                    anim.SetBool("persecution", true);
                    anim.SetBool("attack", false);
                    rb2d.velocity = new Vector2(speedPatrol * ratioPersecution * direction, rb2d.velocity.y);

                    if (player.transform.position.x > transform.position.x) direction = 1;
                    if (player.transform.position.x < transform.position.x) direction = -1;

                    if (distanceToPlayer > persecutionZoneExit || vertDistanceToPlayer >= .5f || vertDistanceToPlayer <= -.5f) behaviour = enemyBehaviourType.passive;
                    if (distanceToPlayer < attackZone && vertDistanceToPlayer <= .5f && vertDistanceToPlayer >= -.5f) behaviour = enemyBehaviourType.attack;
                    break;

                    //Attack
                case enemyBehaviourType.attack:
                    anim.SetBool("attack", true);

                    if (!stillPlaying) AttackSound(); //Gestionar sonido de ataque.
                    time += Time.deltaTime;
                    if (time >= .7f)
                    { 
                        stillPlaying = false;
                        time = 0;
                    }
                    

                    rb2d.velocity = Vector3.zero;

                    if (player.transform.position.x > transform.position.x) direction = 1;
                    if (player.transform.position.x < transform.position.x) direction = -1;

                    if (distanceToPlayer > attackZone) behaviour = enemyBehaviourType.persecution;
                    break;               
            }           
        }

        else if (player == null)
        {
            //Movement
            rb2d.velocity = new Vector2(speedPatrol * direction, rb2d.velocity.y);
            anim.SetBool("move", true);

            //Rotation
            if (transform.position.x < limitPatrolLeft) direction = 1;
            if (transform.position.x > limitPatrolRight) direction = -1;
        }
        transform.localScale = new Vector3(direction, 1, 1);
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Play attack sound, selecting the sound depending on the enemy.
    /// </summary>
    void AttackSound()
    {
        if (gameObject.name == "Chomper") SoundManager.PlaySound("Chomper");
        if (gameObject.name == "Spitter") SoundManager.PlaySound("Spitter");   
        
        stillPlaying = true;       
    }

    /// <summary>
    /// We deactivate it at his death.
    /// </summary>
    /// <returns></returns>
    IEnumerator GetOut()
    {
        yield return new WaitForSeconds(1f);
        this.gameObject.SetActive(false);
    }

    #endregion
}
