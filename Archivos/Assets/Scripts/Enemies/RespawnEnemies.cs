using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnEnemies : MonoBehaviour
{
    #region variables

    public GameObject[] enemies;


    GameManager gm;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        if (gm.itsDead)
        {
            //Makes enemies reappear when the player dies
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].SetActive(true); //Reactivate the enemy
                enemies[i].GetComponent<PinkMonster>().life = 3; //Reset monster life
                enemies[i].GetComponent<PinkMonster>().behaviour = PinkMonster.enemyBehaviourType.passive; //Go intro patrol mode
                enemies[i].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None; //Recover the movement
                enemies[i].GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation; //Block rotation

            }
        }
    }

    #endregion
}
