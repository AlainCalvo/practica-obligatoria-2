using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    #region Variables

    public GameObject pauseMenu;

    public bool gameIsPaused = false;

    #endregion

    #region Main Methods

    private void Start()
    {
        Resume();
    }
    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            pauseMenu.SetActive(!pauseMenu.activeSelf); //Open pause screen.
            gameIsPaused = !gameIsPaused;

            if (gameIsPaused)
            {
                Pause();
            }
            else
                Resume();
        }
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Stop time.
    /// </summary>
    void Pause()
    {
        Time.timeScale = 0;
    }

    /// <summary>
    /// Reset the time and close the pause screen.
    /// </summary>
    public void Resume()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
        gameIsPaused = false;
    }

    /// <summary>
    /// Back to main menu
    /// </summary>
    public void BackToMenu()
    {
        gameIsPaused = false;
        SceneManager.LoadScene(0,LoadSceneMode.Single);
    }

    #endregion
}
