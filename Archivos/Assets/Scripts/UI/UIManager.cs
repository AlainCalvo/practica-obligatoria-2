using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    #region Variables

    DataManager dm;

    public Button load;

    #endregion

    #region Main Methods

    private void Start()
    {
        dm = FindObjectOfType<DataManager>();
        Cursor.lockState = CursorLockMode.None;
    }

    private void Update()
    {
        //If there is no saved data disable the continue button.
        if (dm.active == 1) load.interactable = true;
        else load.interactable = false;
    }

    #endregion

    #region Custom Methods

    /// <summary>
    /// Start new game
    /// </summary>
    public void NewGame()
    {
        PlayerPrefs.DeleteAll(); //Erase saved data
        SceneManager.LoadScene(1, LoadSceneMode.Single); //Load game scene
    }

    /// <summary>
    /// Continue from last checkpoint
    /// </summary>
    public void Continue()
    {
        if (dm.active == 1) SceneManager.LoadScene(1, LoadSceneMode.Single); //If there is saved data, load the game scene with it.
        else Debug.Log("No hay datos guardados.");
    }

    /// <summary>
    /// Leave the game
    /// </summary>
    public void CloseGame()
    {
        Application.Quit();
    }

    /// <summary>
    /// Load Credit screen
    /// </summary>
    public void Credits()
    {
        SceneManager.LoadScene(3, LoadSceneMode.Single);
    }

#endregion
}
