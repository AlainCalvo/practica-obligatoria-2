using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    #region Variables

    [Header("---REFERENCIAS---")]
    public GameObject inventory;
    public GameObject weapon;
    public GameObject key1;
    public GameObject key2;

    GameManager gm;
    PlayerController pc;

    #endregion

    #region Main Methods

    private void Start()
    {
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        pc = FindObjectOfType<PlayerController>();

        //Open or close the inventory.
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventory.SetActive(!inventory.activeSelf);
        }

        //Activate in the inventory what has been obtained.
        if (gm.firstKey) key1.SetActive(true);
        else key1.SetActive(false);

        if (gm.secondKey) key2.SetActive(true);
        else key2.SetActive(false);

        if (pc.weapon) weapon.SetActive(true);
        else weapon.SetActive(false);
    }

    #endregion
}
